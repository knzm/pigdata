GCC = gcc

CFLAGS = -g -O2 -std=c99 -Wall -fopenmp -fPIC
LDFLAGS = -g -O2 -fopenmp

all: pigdata pigdata_mp pigdata.so

clean:
	@rm -f pigdata pigdata_mp pigdata.so pigdata.o pigdata_mp.o pigdata_main.o util.o

pigdata: pigdata.o util.o pigdata_main.o
	$(GCC) $(LDFLAGS) -o pigdata pigdata.o util.o pigdata_main.o -lcrypto

pigdata_mp: pigdata_mp.o util.o pigdata_main.o
	$(GCC) $(LDFLAGS) -o pigdata_mp pigdata_mp.o util.o pigdata_main.o -lcrypto

pigdata.so: pigdata.o util.o
	$(GCC) $(LDFLAGS) -shared -o pigdata.so pigdata.o util.o -lcrypto

.c.o:
	$(GCC) $(CFLAGS) -c $<
