# -*- coding: utf-8 -*-

from __future__ import print_function

import hashlib
import struct
import time
import sys


def getdata_sub(q):
    digest = hashlib.sha1(str(q)).digest()
    return struct.unpack(">" + "H" * 10, digest)


def getdata(index):
    """
    - 引数 index は 0 以上の整数である。
    - 以下では index を 10 で割って小数以下を切り捨てた整数値を q と呼び、
      index を 10 で割った余りを r と呼ぶ。すなわち index = 10q + r である
      (0 ≦ r < 10)。

    - getdata は以下の手順で戻り値を計算する。

      - q を十進表記し、ASCII コードを使って文字列 s を作る。
      - 文字列 s には、C 言語にあるような終端の '\0' は含まれない。
      - 文字列 s をバイト列と見なし、メッセージダイジェストアルゴリズム
        SHA-1 を用いて 20 バイトのダイジェスト値 d[0]...d[19] を求める。
      - 2 バイト d[2r] と d[2r + 1] を符号なし 16 ビット整数 data と見なす
        (ここで、d[2r] が上位 8 ビットであり、d[2r + 1] が下位 8 ビットとする)。
      - このようにして得た data を getdata(index) の戻り値とする。
    """
    q = index // 10
    r = index % 10
    return getdata_sub(q)[r]


def count_range(start, end, skips):
    # sizeof {x | x in [start, end), x = skips * n, n = 0, 1, 2, ...}
    n1 = (start + skips - 1) // skips
    n2 = (end + skips - 1) // skips
    return n2 - n1


def format_eta(t, ratio):
    if ratio < 1:
        s = t * (1 - ratio) / ratio
    else:
        s = 0
    m = s // 60; s -= m * 60
    h = m // 60; m -= h * 60
    return "%02d:%02d:%02d" % (h, m, s)


def getsign(count, skips, progress=False):
    """
    - 引数 count は 1 以上の整数である。
    - 引数 skips は 1 以上の整数である。
    - getsign は以下の手順で戻り値を計算する。
      - 変数 index を 0 以上 count 未満の範囲で 1 ずつ増加させて
        getdata(index) を呼ぶ。
      - 得られた count 個のデータを昇順でソートすると得られる数列を
        sorted[0], ..., sorted[count − 1] と呼ぶ。昇順なので、以下が成り立つ。
        sorted[0] ≦ sorted[1] ≦ · · · ≦ sorted[count − 1]
      - k を、0 ≦ k < count かつ skips の倍数の範囲で動かしたときの
        sorted[k] の総和を getsign(count, skips) の戻り値とする
        (すなわち、k を 0, 1skips, 2skips, 3skips, . . . のように動かして
        (ただし count 未満)、 sorted[k] の総和を取るということである)。
    """
    start_time = time.time()

    table = [0] * 0x10000
    for i in xrange(count // 10):
        for n in getdata_sub(i):
            table[n] += 1
        if progress:
            if i % 100000 == 0:
                cur_time = time.time()
                t = cur_time - start_time
                ratio = float(i + 1) / (count // 10)
                if ratio > 0.001:
                    eta = "ETA: " + format_eta(t, ratio)
                else:
                    eta = "ETA: --------"
                print("count=%d/%d (%5.2f%%) in %.2f sec [%s]"
                      % ((i + 1) * 10, count, ratio * 100, t, eta),
                      end='\r', file=sys.stderr)
    for i in xrange((count // 10) * 10, count):
        n = getdata(i)
        table[n] += 1

    sign = 0
    k = 0
    for n in xrange(0x10000):
        c = table[n]
        if c == 0:
            continue
        sign += count_range(k, k + c, skips) * n
        k += c

    if progress:
        print()

    return sign


def main():
    # print(getsign(100, 10, progress=True))
    # print(getsign(1000000000, 1000, progress=True))
    print(getsign(107374182400, 16777216, progress=True))


if __name__ == '__main__':
    main()
