# -*- coding: utf-8 -*-

import unittest

from pigdata import getdata, getsign, count_range


class GetDataTest(unittest.TestCase):
    def test_getdata_0(self):
        self.assertEqual(getdata(0), 46680)

    def test_getdata_123456(self):
        self.assertEqual(getdata(123456), 50701)


class GetSignTest(unittest.TestCase):
    def test_sign_100_10(self):
        self.assertEqual(getsign(100, 10), 284067)


class CountRangeTest(unittest.TestCase):
    def test_count_range_0_0_10(self):
        # []
        self.assertEqual(count_range(0, 0, 10), 0)

    def test_count_range_0_1_10(self):
        # [0]
        self.assertEqual(count_range(0, 1, 10), 1)

    def test_count_range_0_20_10(self):
        # [0, 10]
        self.assertEqual(count_range(0, 20, 10), 2)

    def test_count_range_5_15_10(self):
        # [10]
        self.assertEqual(count_range(5, 15, 10), 1)

    def test_count_range_5_25_5(self):
        # [5, 10, 15, 20]
        self.assertEqual(count_range(5, 25, 5), 4)


if __name__ == '__main__':
    unittest.main()
