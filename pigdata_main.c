#include <stdio.h>

#include "pigdata.h"

int main(int argc, char *argv[]) {
  // long sign = getsign(100, 10);
  // long sign = getsign(100000000, 1000);
  long sign = getsign(107374182400, 16777216);
  printf("%ld\n", sign);

  return 0;
}
