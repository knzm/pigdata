import os

from fabric.api import env, run, sudo, put, local

source_files = [
    "pigdata.py",
    "pigdata_main.c",
    "pigdata.h",
    "pigdata.c",
    "pigdata_mp.c",
    "util.h",
    "util.c",
    "Makefile",
    ]

here = os.path.dirname(os.path.abspath(__file__))
src_dir = os.path.dirname(here)


def unquote(s):
    if ((s.startswith('"') and s.endswith('"')) or
        (s.startswith("'") and s.endswith("'"))):
        return s[1:-1]
    else:
        return s

def apt_install(*packages):
    sudo("apt-get install -qq -y %s" % " ".join(packages))

def up():
    local("vagrant up --provider aws")

def destroy():
    local("vagrant destroy")

def vagrant():
    from StringIO import StringIO
    from paramiko.config import SSHConfig
    from time import sleep
    from fabric.network import join_host_strings

    content = local("vagrant ssh-config", capture=True)
    config = SSHConfig()
    config.parse(StringIO(content))

    host_info = config.lookup("default")

    key_filenames = host_info.get('identityfile', None)
    if key_filenames:
        env.key_filename = [os.path.expanduser(unquote(key_filename))
                            for key_filename in key_filenames]

    user = host_info["user"]
    host = host_info["hostname"]
    port = host_info["port"]
    host_string = join_host_strings(user, host, port)
    env.update({
            "host_string": host_string,
            "user": user,
            "host": host,
            "port": port,
            })

def init():
    sudo("date > /etc/vagrant_box_build_time")

def update_packages():
    sudo("apt-get -qq -y update")
    sudo("apt-get -qq -y upgrade")

def install_packages():
    sudo("apt-get -qq -y update")
    apt_install("build-essential") # gcc, make
    apt_install("libssl-dev")
    # apt_install("sqlite3", "libsqlite3-dev")
    # apt_install("libxml2-dev", "libxslt1-dev")
    # apt_install("zlib1g-dev")
    # apt_install("libreadline-dev")
    # apt_install("python-virtualenv")
    # apt_install("curl", "vim")
    # apt_install("git-core", "mercurial")

def clear_package_cache():
    sudo("apt-get clean")

def packages():
    update_packages()
    install_packages()
    clear_package_cache()

def deploy():
    for filename in source_files:
        path = os.path.join(src_dir, filename)
        if os.path.exists(path):
            put(path)
    run("make clean")
    run("make")

def main():
    run("time python pigdata.py")

def all():
    up()
    vagrant()
    packages()
    deploy()
    main()
    destroy()
