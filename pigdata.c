#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <time.h>

#include "pigdata.h"
#include "util.h"

inline void getdata_sub(long q, unsigned char buf[20]) {
  char ibuf[256];
  snprintf(ibuf, sizeof(ibuf), "%ld", q);
  sha1((unsigned char*)ibuf, strlen(ibuf), buf);
}

int getdata(long index) {
  int q = index / 10;
  int r = index % 10;
  unsigned char buf[20];
  getdata_sub(q, buf);
  const unsigned char *p = &buf[r * 2];
  return GET_USHORT(p);
}

long count_range(long start, long end, long skips) {
  long n1 = (start + skips - 1) / skips;
  long n2 = (end + skips - 1) / skips;
  return n2 - n1;
}

void getsign_sub(long start, long end, long *table, bool show_progress) {
  const int table_size = 0x10000;
  bzero(table, sizeof(long) * table_size);

  const int block_size = 10;
  long block_start = (start + block_size - 1) / block_size;
  long block_end = end / block_size;
  long data_size = end - start;

  time_t start_time = time(NULL);
  long done = 0;

  for (long i = start; i < block_start * block_size; i++) {
    int n = getdata(i);
    table[n] += 1;
  }
  for (long i = block_start; i < block_end; i++) {
    if (show_progress && (i % 10000 == 0)) {
      time_t cur_time = time(NULL);
      int t = cur_time - start_time;
      double ratio = ((double) done) / data_size;
      char eta[16];
      if (ratio > 0.001) {
        format_eta(t, ratio, eta, sizeof(eta));
      }
      else {
        sprintf(eta, "--------");
      }
      printf("count=%ld/%ld (%.2f%%) in %d sec [ETA: %s]\r",
             done, data_size, ratio * 100, t, eta);
    }

    unsigned char buf[2 * block_size];
    getdata_sub(i, buf);
    for (int j = 0; j < 2 * block_size; j += 2) {
      int n = GET_USHORT(&buf[j]);
      table[n] += 1;
    }

    done += block_size;
  }
  for (long i = block_end * block_size; i < end; i++) {
    int n = getdata(i);
    table[n] += 1;
  }

  if (show_progress) {
    printf("\n");
  }
}

long getsign(long count, long skips) {
  const int table_size = 0x10000;
  long table[table_size];

  getsign_sub(0, count, table, true);

  long sign = 0;
  long k = 0;
  for (size_t n = 0; n < table_size; n++) {
    long c = table[n];
    if (c > 0) {
      sign += count_range(k, k + c, skips) * n;
      k += c;
    }
  }

  return sign;
}
