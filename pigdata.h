void getdata_sub(long q, unsigned char buf[20]);
int getdata(long index);
long count_range(long start, long end, long skips);
long getsign(long count, long skips);

#define GET_USHORT(p) ((((unsigned char*)(p))[0] << 8) | ((unsigned char*)(p))[1])

#ifdef _OPENMP
#include <omp.h>
#define NUM_THREADS() omp_get_max_threads()
#define THREAD_ID() omp_get_thread_num()
#else
#define NUM_THREADS() 1
#define THREAD_ID() 0
#endif
