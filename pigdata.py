# -*- coding: utf-8 -*-

from __future__ import print_function

import ctypes
import os
import sys
import multiprocessing
import time

pigdata_so = ctypes.CDLL("./pigdata.so")


def count_range(start, end, skips):
    # sizeof {x | x in [start, end), x = skips * n, n = 0, 1, 2, ...}
    n1 = (start + skips - 1) // skips
    n2 = (end + skips - 1) // skips
    return n2 - n1


def format_eta(t, ratio):
    if ratio < 1:
        s = t * (1 - ratio) / ratio
    else:
        s = 0
    m = s // 60; s -= m * 60
    h = m // 60; m -= h * 60
    return "%02d:%02d:%02d" % (h, m, s)


def getsign_sub(start, end):
    buf = (ctypes.c_long * 0x10000)()
    pigdata_so.getsign_sub(start, end, buf, False)
    return buf


def getsign_helper(args):
    # print("pid=%d, args=%r" % (os.getpid(), args))
    buf = getsign_sub(*args)
    return os.getpid(), list(buf)


class AbstractScheduler(object):
    def get_ranges(self):
        raise NotImplementedError


class FixedSizeScheduler(AbstractScheduler):
    def __init__(self, data_size, processes):
        self.data_size = data_size
        self.processes = processes
        self.chunk_size = self._get_chunk_size(data_size, processes)
        self.processed_size = 0

    def _get_chunk_size(self, data_size, min_chunks):
        return (data_size + min_chunks - 1) // min_chunks

    def get_ranges(self):
        ranges = []
        for n in xrange(0, self.data_size, self.chunk_size):
            ranges.append((n, n + self.chunk_size))
        n = (self.data_size // self.chunk_size) * self.chunk_size
        if n < self.data_size:
            ranges.append((n, self.data_size))
        return ranges

    def processed(self):
        self.processed_size += self.chunk_size

    def get_ratio(self):
        return float(self.processed_size) / self.data_size


class ResponsiveScheduler(FixedSizeScheduler):
    max_chunk_size = 10000000

    def _get_chunk_size(self, data_size, min_chunks):
        chunk_size = super(ResponsiveScheduler, self)._get_chunk_size(
            data_size, min_chunks)
        if chunk_size > self.max_chunk_size:
            chunk_size = self.max_chunk_size
        return chunk_size


class Watcher(object):
    def __init__(self):
        self.start()

    def start(self):
        self.start_time = time.time()

    def get_tick_count(self):
        cur_time = time.time()
        return cur_time - self.start_time


def getsign(count, skips, processes=None, progress=True):
    if processes:
        table = [0] * 0x10000

        if progress:
            scheduler = ResponsiveScheduler(count, processes)
        else:
            scheduler = FixedSizeScheduler(count, processes)

        ranges = scheduler.get_ranges()
        watcher = Watcher()

        pool = multiprocessing.Pool(processes=processes)
        for pid, buf in pool.imap_unordered(getsign_helper, ranges):
            for i, x in enumerate(buf):
                table[i] += x
            scheduler.processed()
            if progress:
                t = watcher.get_tick_count()
                ratio = scheduler.get_ratio()
                if ratio > 0.001:
                    eta = "ETA: " + format_eta(t, ratio)
                else:
                    eta = "ETA: --------"
                print("[%d] count=%d/%d (%5.2f%%) in %.2f sec [%s]"
                      % (pid, scheduler.processed_size, scheduler.data_size,
                         ratio * 100, t, eta),
                      end='\r', file=sys.stderr)
        if progress:
            print("\n")

    else:
        table = list(getsign_sub(0, count))

    sign = 0
    k = 0
    for n, c in enumerate(table):
        if c == 0:
            continue
        sign += count_range(k, k + c, skips) * n
        k += c

    return sign


def main():
    if len(sys.argv) > 1:
        processes = int(sys.argv[1])
    else:
        processes = multiprocessing.cpu_count()
    # sign = getsign(1000000000, 1000, processes)
    sign = getsign(107374182400, 16777216, processes)
    print(sign)


def perf():
    try:
        import cProfile as profile
    except ImportError:
        import profile
    import pstats
    profile.run('main()', 'perf.prof')
    p = pstats.Stats("perf.prof")
    p.sort_stats("cum").print_stats(10)


if __name__ == '__main__':
    # perf()
    main()
