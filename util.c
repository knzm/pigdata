#include <stdio.h>
#include <openssl/sha.h>

#include "util.h"

void format_eta(int t, double ratio, char eta[], size_t size) {
  int s = (ratio < 1) ? t * (1 - ratio) / ratio : 0;
  int m = s / 60; s -= m * 60;
  int h = m / 60; m -= h * 60;
  snprintf(eta, size, "%02d:%02d:%02d", h, m, s);
}

#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
void sha1(const unsigned char *data, size_t data_size, unsigned char *result) {
  SHA_CTX encoder;
  SHA1_Init(&encoder);
  SHA1_Update(&encoder, data, data_size);
  SHA1_Final(result, &encoder);
}
#pragma GCC diagnostic warning "-Wdeprecated-declarations"
